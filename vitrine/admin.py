from django.contrib import admin
from .models import *

admin.site.register(Categorie)

admin.site.register(Creation)
admin.site.register(Contact)
admin.site.register(SousCategorie)


# Register your models here.
