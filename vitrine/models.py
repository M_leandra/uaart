from django.db import models


# Create your models here.

class Categorie(models.Model):
    """cette classe parmet de crea"""
    nom = models.CharField(max_length=250)
    code = models.CharField(max_length=250)
    def __str__(self):
        return "{}".format(self.code)

class SousCategorie(models.Model):
    nom = models.CharField(max_length=250)
    code = models.CharField(max_length=250)
    categorie = models.ForeignKey(Categorie, on_delete=models.CASCADE)

    def __str__(self):
        return "{}".format(self.code)

class Creation(models.Model):
    nom = models.CharField(max_length=250)
    image = models.ImageField(upload_to="creation/")
    description = models.TextField()
    categorie = models.ForeignKey(Categorie, on_delete=models.CASCADE)
    sous_categorie = models.ForeignKey(SousCategorie, on_delete=models.CASCADE)
    create_at = models.DateTimeField(auto_now=True)
    def __str__(self):
        return "{}".format(self.nom)




class  Contact(models.Model):
    nom = models.CharField(max_length=250)
    subject = models.CharField(max_length=250)
    email = models.EmailField()
    message = models.TextField()
    def __str__(self):
        return "{}---> {}--->{}".format(self.nom,self.email,self.message)

