from django.urls import path
from django.contrib.auth.decorators import login_required
from .views import *


urlpatterns = [
 path('',  VitrineViews.as_view(), name='accueil'),
 path('detail/<int:id>', detail_view, name='details'),
 path('categorie/<str:nom>', categirie_views, name='categorie'),
 path('liste_categorie/', list_ctg, name='list_ctg'),
 path('liste_souscategorie/', list_sctg, name='list_sctg'),
 path('add_categorie/', ajouter_ctg, name='add_ctg'),
 path('upd_categorie/<int:id>', update_ctg, name='upd_ctg'),
 path('add_souscategorie/', ajouter_sctg, name='add_sctg'),
 path('del_souscategorie/<int:id>', del_sctg, name='del_sctg'),
 path('del_categorie/<int:id>', del_ctg, name='del_ctg'),
 path('upd_souscategorie/<int:id>', update_sctg, name='upd_sctg'),
 path('liste_creations/',list_creation,name="list_creation"),
 path('add_creations/',creercreation,name="add_creation"),
 path('del_creations/<int:id>',del_creation,name="del_creation"),
 path('login/',login_views,name='login'),
 path('logout/',logout_views,name='logout')

]
