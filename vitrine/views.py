from django.shortcuts import render, get_object_or_404,HttpResponse,redirect,HttpResponseRedirect
from .models import *
from .forms import *
from django.views import View
from django.urls import reverse_lazy
from django.core.paginator import Paginator,EmptyPage
from django.core.mail import send_mass_mail,send_mail
from django.contrib.auth import authenticate, login, logout
from django.conf import settings


# Create your views here.


class VitrineViews(View):
    def get(self,request):
        creations = Creation.objects.all()
        categories = Categorie.objects.all()
        scategories = SousCategorie.objects.all()
        ctx = {'categories': categories,
               'creations': creations,
               'scategories': scategories}
        return render(request,'vitrine/index.html',ctx)

    def post(self,request):
        nom = request.POST.get('nom')
        email = request.POST.get('email')
        sujet = request.POST.get('subject')
        message = request.POST.get('message')
        Contact.objects.create(nom=nom, email=email, subject=sujet, message=message)
        send_mail(
            sujet,
            message,
            email,
            ['leandramakamte@yahoo.fr'],
            fail_silently=True,
        )

        send_mail(
            'Accusé de reception',
            'Nous avons recu votre message.'+ nom + email,
            'leandramakamte@yahoo.fr',
            [email],
            fail_silently=True,
        )
        return redirect('accueil')


######################################################################################################################
def detail_view(request, id):
        creation = Creation.objects.get(pk=id)
        ctx = {
               'creation': creation}
        return render(request, 'vitrine/details.html', ctx)

def categirie_views(request, nom):

    categorie = get_object_or_404(Categorie,nom=nom)
    if categorie:
        creations = Creation.objects.filter(categorie=categorie)
        scategories = SousCategorie.objects.filter(categorie=categorie)
        ctx = {'scategories': scategories,
               'creations': creations,
               'categorie': categorie}
        return render(request, 'vitrine/categorie.html', ctx)
##############################################################################
'''    '''
def list_ctg(request):

    categiries = Categorie.objects.all()
    ctx = {'categories': categiries}
    return render(request, 'vitrine/listcategorie.html', ctx)

def ajouter_ctg(request):
    if request.method=='POST':
        nom =request.POST.get('nom')
        code =request.POST.get('code')
        Categorie.objects.create(nom=nom,code=code)

        return redirect('list_ctg')
    ctx={'action': 'add'}
    return render(request, 'vitrine/gestioncategorie.html', ctx)

def del_ctg(request,id):
    ctg = Categorie.objects.get(id=id)
    if request.method == 'POST':
        ctg.delete()
        return redirect('list_ctg')
    ctx = {'action': 'del',
           'ctg': ctg }
    return render(request, 'vitrine/gestioncategorie.html', ctx)

def update_ctg(request,id):
    ctg = Categorie.objects.get(id=id)

    if request.method == 'POST':
        nom = request.POST.get('nom')
        code = request.POST.get('code')
        if code:
            ctg.nom = code
        elif nom:
            ctg.nom = nom
        ctg.save()
        return redirect('list_ctg')
    ctx = {'action': 'upd',
               'ctg': ctg}
    return render(request, 'vitrine/gestioncategorie.html', ctx)
#############################################################################################################################################################

def list_sctg(request):

    scategories = SousCategorie.objects.all()
    ctx = {'scategories': scategories}
    return render(request, 'vitrine/listsousctg.html', ctx)

def ajouter_sctg(request):
    categiries = Categorie.objects.all()
    if request.method =='POST':
        nom =request.POST.get('nom')
        code =request.POST.get('code')
        categorie =request.POST.get('categori')
        print('\n ici ',categorie,'\n la')
        SousCategorie.objects.create(nom=nom, code=code, categorie_id=categorie)

        return redirect('list_sctg')
    ctx={'action': 'add',
         'ctgs':categiries}
    return render(request, 'vitrine/gestionsousctg.html', ctx)

def del_sctg(request,id):
    sctg = SousCategorie.objects.get(id=id)
    if request.method == 'POST':
        sctg.delete()
        return redirect('list_sctg')
    ctx = {'action': 'del',
           'sctg': sctg }
    return render(request, 'vitrine/gestionsousctg.html', ctx)

def update_sctg(request,id):
    sctg = SousCategorie.objects.get(id=id)
    categiries = Categorie.objects.all()
    if request.method == 'POST':
        nom = request.POST.get('nom')
        code = request.POST.get('code')
        ctg = request.POST.get('categori')
        if nom:
            sctg.nom = nom
        if code:
            sctg.code = code
        if ctg:
            sctg.categorie = Categorie.objects.get(id=ctg)
        sctg.save()

        return redirect('list_sctg')
    ctx = {'action': 'upd',
            'sctg': sctg,
            'categories': categiries}
    return render(request, 'vitrine/gestionsousctg.html', ctx)
########################################################################################################################################
def list_creation(request):
    creations = Creation.objects.all()
    ctx = {"creations" : creations}
    return render(request,'vitrine/liste_creation.html',ctx)

def creercreation(request):
    categories = Categorie.objects.all()
    scategories = SousCategorie.objects.all()
    if request.method == 'POST' :
        nom = request.POST.get('nom')
        ctg = request.POST.get('categorie')
        sctg = request.POST.get('souscategorie')
        desc = request.POST.get('description')
        img = request.POST.get('image')
        if img:
            Creation.objects.create(nom=nom, image=img, description=desc, categorie_id=ctg, sous_categorie_id=sctg)
        return redirect('list_creation')
    ctx = {'categories': categories,
           'scategories': scategories ,
           'action':'add'}
    return render(request,'vitrine/gestion_creation.html',ctx)

def del_creation(request,id):
    creation = Creation.objects.get(id=id)
    if request.method == 'POST':
        creation.delete()
        return redirect('list_creation')
    ctx = {'action': 'del',
           'creation': creation }
    return render(request, 'vitrine/gestion_creation.html', ctx)

def  login_views(request):

    if request.method =='POST':
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(username=username, password=password)
        if user is not None:
            login(request, user)
           
            return redirect('list_creation')


    return render(request,'vitrine/login.html')

def logout_views(request):
    user = request.user
    if request.method == 'POST':
        logout(request)
        return redirect('accueil')
    ctx = {'user': user}
    return render(request, 'vitrine/admin.html', ctx)